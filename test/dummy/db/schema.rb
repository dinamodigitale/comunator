# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180314150145) do

  create_table "comunator_cap", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comunator_cap_comuni", id: false, force: :cascade do |t|
    t.integer "comunator_cap_id",    null: false
    t.integer "comunator_comuni_id", null: false
  end

  create_table "comunator_comuni", force: :cascade do |t|
    t.integer  "provincia_id"
    t.string   "nome"
    t.integer  "zona_id"
    t.integer  "regione_id"
    t.string   "sigla"
    t.string   "codice_catastale"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["provincia_id"], name: "index_comunator_comuni_on_provincia_id"
    t.index ["regione_id"], name: "index_comunator_comuni_on_regione_id"
    t.index ["zona_id"], name: "index_comunator_comuni_on_zona_id"
  end

  create_table "comunator_provincia", force: :cascade do |t|
    t.string   "nome"
    t.string   "sigla"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comunator_regione", force: :cascade do |t|
    t.string   "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comunator_zona", force: :cascade do |t|
    t.string   "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end

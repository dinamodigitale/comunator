class CreateComunatorTables < ActiveRecord::Migration[5.0]
  def change
    create_table :comunator_comuni do |t|
      t.belongs_to :provincia
      t.string :nome
      t.belongs_to :zona
      t.belongs_to :regione
      t.string :sigla
      t.string :codice_catastale
      t.timestamps
    end

    create_table :comunator_provincia do |t|
      t.string :nome
      t.string :sigla
      t.timestamps
    end

    create_table :comunator_regione do |t|
      t.string :nome
      t.timestamps
    end

    create_table :comunator_zona do |t|
      t.string :nome
      t.timestamps
    end

    create_table :comunator_cap do |t|
      t.timestamps :created_at
    end

    create_join_table :comunator_cap, :comunator_comuni
  end
end

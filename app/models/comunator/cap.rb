module Comunator
  class Cap < ApplicationRecord
    self.table_name = "comunator_cap"
    has_and_belongs_to_many :comuni, class_name: "Comunator::Comune", association_foreign_key: :comunator_comuni_id, foreign_key: :comunator_cap_id
  end
end

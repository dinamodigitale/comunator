module Comunator
  class Regione < ApplicationRecord
    self.table_name = "comunator_regione"
    has_many :comuni, class_name: "Comunator::Comune"
    has_many :provincie, class_name: "Comunator::Provincia", through: :comuni, source: :provincia
  end
end

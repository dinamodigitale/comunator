module Comunator
  class Comune < ApplicationRecord
    self.table_name = "comunator_comuni"
    has_and_belongs_to_many :cap, class_name: "Comunator::Cap", association_foreign_key: :comunator_cap_id, foreign_key: :comunator_comuni_id
    belongs_to :provincia, class_name: "Comunator::Provincia"
    belongs_to :regione, class_name: "Comunator::Regione"
    belongs_to :zona, class_name: "Comunator::Zona"

    default_scope { includes([:provincia, :regione, :zona]) }
  end
end

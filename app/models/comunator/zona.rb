module Comunator
  class Zona < ApplicationRecord
    self.table_name = "comunator_zona"
    has_many :comuni, class_name: "Comunator::Comune"
  end
end

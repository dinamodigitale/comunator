module Comunator
  class Provincia < ApplicationRecord
    self.table_name = "comunator_provincia"
    has_many :comuni, class_name: "Comunator::Comune"
  end
end

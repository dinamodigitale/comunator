$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "comunator/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "comunator"
  s.version     = Comunator::VERSION
  s.authors     = ["Manuel Burato"]
  s.email       = ["contact@manuelburato.it"]
  s.homepage    = "https://www.dinamodigitale.it"
  s.summary     = "Comuni Italiani"
  s.description = "Comuni Italiani"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "> 5.0.0"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "byebug"
end

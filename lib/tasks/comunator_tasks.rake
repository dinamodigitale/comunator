desc "Explaining what the task does"
namespace :comunator do
  task update: :environment do
    require "open-uri"
    url = "https://raw.githubusercontent.com/matteocontrini/comuni-json/master/comuni.json"
    puts "Tryng to download file from #{url}"
    comuni_raw_content = open(url).read
    puts "Download completed."
    comuni_content = JSON.parse(comuni_raw_content)
    comuni_content_count = comuni_content.length
    puts "Parsed #{comuni_content_count} comuni"

    puts "Truncating DB!"
    Comunator::Cap.delete_all()
    Comunator::Comune.delete_all()
    Comunator::Provincia.delete_all()
    Comunator::Regione.delete_all()
    Comunator::Zona.delete_all()

    cache_provincia = {}
    cache_cap = {}
    cache_zona = {}
    cache_regione = {}

    comunator_progress = Comunator::ProgressBar.new("Populating DB", comuni_content_count)

    # index = 0
    comuni_content.each do |comune|
      # index += 1
      comune = comune.with_indifferent_access
      comunator_progress.inc

      # print "\rInserting (#{index}/#{comuni_content_count}) #{comune[:nome].ljust(40)} (#{comune[:sigla]}) - #{comune[:regione][:nome].ljust(40)}"
      comune[:id] = comune[:codice]
      comune.delete :codice

      zona = comune[:zona]
      zona[:id] = zona[:codice].to_i
      zona.delete :codice

      regione = comune[:regione]
      regione[:id] = regione[:codice].to_i
      regione.delete :codice

      provincia = comune[:provincia]
      provincia[:nome] = if provincia[:nome].empty?
                           comune[:cm][:nome]
                         else
                           provincia[:nome]
                         end

      provincia[:id] = provincia[:codice].to_i
      provincia[:sigla] = comune[:sigla]
      provincia.delete :codice

      comune.delete :cm
      comune.delete :provincia
      comune.delete :zona
      comune.delete :regione

      comune[:provincia] = if cache_provincia[provincia[:id]]
                             cache_provincia[provincia[:id]]
                           else
                             cache_provincia[provincia[:id]] = Comunator::Provincia.create(provincia)
                           end

      comune[:regione] = if cache_regione[regione[:id]]
                           cache_regione[regione[:id]]
                         else
                           cache_regione[regione[:id]] = Comunator::Regione.create(regione)
                         end

      comune[:zona] = if cache_zona[zona[:id]]
                        cache_zona[zona[:id]]
                      else
                        cache_zona[zona[:id]] = Comunator::Zona.create(zona)
                      end

      comune[:codice_catastale] = comune[:codiceCatastale]
      comune.delete :codiceCatastale

      comune[:cap].map! do |cap|
        if cache_cap[cap]
          cache_cap[cap]
        else
          cache_cap[cap] = Comunator::Cap.create(id: cap)
        end
      end

      comune = Comunator::Comune.create(comune)
    end
    comunator_progress.finish
    puts ""
    puts "Import done!"
  end

  # task manual_update: :environment do
  #
  # end
end
